﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace s16_extension
{
    [StaticConstructorOnStartup]
    public class RangedShieldBelt : Apparel
    {
        private static readonly Material BubbleMat = MaterialPool.MatFrom("Other/ShieldBubble", ShaderDatabase.Transparent);
        private int ticksToReset = -1;
        private int lastKeepDisplayTick = -9999;
        private int lastAbsorbDamageTick = -9999;
        private int StartingTicksToReset = 3200;
        private float EnergyOnReset = 0.2f;
        private float EnergyLossPerDamage = 0.033f;
        private int KeepDisplayingTicks = 1000;
        private float ApparelScorePerEnergyMax = 0.25f;
        private float energy;
        private Vector3 impactAngleVect;

        private float EnergyMax
        {
            get
            {
                return this.GetStatValue(StatDefOf.EnergyShieldEnergyMax, true);
            }
        }

        private float EnergyGainPerTick
        {
            get
            {
                return this.GetStatValue(StatDefOf.EnergyShieldRechargeRate, true) / 60f;
            }
        }

        public float Energy
        {
            get
            {
                return this.energy;
            }
        }

        public ShieldState ShieldState
        {
            get
            {
                return this.ticksToReset > 0 ? ShieldState.Resetting : ShieldState.Active;
            }
        }

        private bool ShouldDisplay
        {
            get
            {
                Pawn wearer = this.Wearer;
                return wearer.Spawned && !wearer.Dead && !wearer.Downed && (wearer.InAggroMentalState || wearer.Drafted || (wearer.Faction.HostileTo(Faction.OfPlayer) && !wearer.IsPrisoner || Find.TickManager.TicksGame < this.lastKeepDisplayTick + this.KeepDisplayingTicks));
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<float>(ref this.energy, "energy", 0.0f, false);
            Scribe_Values.Look<int>(ref this.ticksToReset, "ticksToReset", -1, false);
            Scribe_Values.Look<int>(ref this.lastKeepDisplayTick, "lastKeepDisplayTick", 0, false);
        }

        public override IEnumerable<Gizmo> GetWornGizmos()
        {
            if (Find.Selector.SingleSelectedThing == this.Wearer)
                yield return (Gizmo)new Gizmo_RangedShieldStatus()
                {
                    shield = this
                };
        }

        public override float GetSpecialApparelScoreOffset()
        {
            return this.EnergyMax * this.ApparelScorePerEnergyMax;
        }

        public override void Tick()
        {
            base.Tick();
            if (this.Wearer == null)
                this.energy = 0.0f;
            else if (this.ShieldState == ShieldState.Resetting)
            {
                --this.ticksToReset;
                if (this.ticksToReset > 0)
                    return;
                this.Reset();
            }
            else
            {
                if (this.ShieldState != ShieldState.Active)
                    return;
                this.energy += this.EnergyGainPerTick;
                if ((double)this.energy > (double)this.EnergyMax)
                    this.energy = this.EnergyMax;
            }
        }

        public override bool CheckPreAbsorbDamage(DamageInfo dinfo)
        {
            if ((uint)this.ShieldState > 0U)
                return false;
            if (dinfo.Def == DamageDefOf.EMP)
            {
                this.energy = 0.0f;
                this.Break();
                return false;
            }
            if (!dinfo.Def.isRanged && !dinfo.Def.isExplosive)
                return false;
            this.energy -= dinfo.Amount * this.EnergyLossPerDamage;
            if ((double)this.energy < 0.0)
                this.Break();
            else
                this.AbsorbedDamage(dinfo);
            return true;
        }

        public void KeepDisplaying()
        {
            this.lastKeepDisplayTick = Find.TickManager.TicksGame;
        }

        private void AbsorbedDamage(DamageInfo dinfo)
        {

            SoundDefOf.EnergyShield_AbsorbDamage.PlayOneShot((SoundInfo)new TargetInfo(this.Wearer.Position, this.Wearer.Map, false));
            this.impactAngleVect = Vector3Utility.HorizontalVectorFromAngle(dinfo.Angle);
            Vector3 loc = this.Wearer.TrueCenter() + this.impactAngleVect.RotatedBy(180f) * 0.5f;
            float scale = Mathf.Min(10f, (float)(2.0 + (double)dinfo.Amount / 10.0));
            FleckMaker.Static(loc, this.Wearer.Map, FleckDefOf.ExplosionFlash, scale);
            int num = (int)scale;
            for (int index = 0; index < num; ++index)
                FleckMaker.ThrowDustPuff(loc, this.Wearer.Map, Rand.Range(0.8f, 1.2f));
            this.lastAbsorbDamageTick = Find.TickManager.TicksGame;
            this.KeepDisplaying();
        }

        private void Break()
        {
            SoundDefOf.EnergyShield_Broken.PlayOneShot((SoundInfo)new TargetInfo(this.Wearer.Position, this.Wearer.Map, false));
            FleckMaker.Static(this.Wearer.TrueCenter(), this.Wearer.Map, FleckDefOf.ExplosionFlash, 12f);
            for (int index = 0; index < 6; ++index)
                FleckMaker.ThrowDustPuff(this.Wearer.TrueCenter() + Vector3Utility.HorizontalVectorFromAngle((float)Rand.Range(0, 360)) * Rand.Range(0.3f, 0.6f), this.Wearer.Map, Rand.Range(0.8f, 1.2f));
            this.energy = 0.0f;
            this.ticksToReset = this.StartingTicksToReset;
        }

        private void Reset()
        {
            if (this.Wearer.Spawned)
            {
                SoundDefOf.EnergyShield_Reset.PlayOneShot((SoundInfo)new TargetInfo(this.Wearer.Position, this.Wearer.Map, false));
                FleckMaker.ThrowLightningGlow(this.Wearer.TrueCenter(), this.Wearer.Map, 3f);
            }
            this.ticksToReset = -1;
            this.energy = this.EnergyOnReset;
        }

        public override void DrawWornExtras()
        {
            if (this.ShieldState != ShieldState.Active || !this.ShouldDisplay)
                return;
            float num1 = Mathf.Lerp(1.2f, 1.55f, this.energy);
            Vector3 drawPos = this.Wearer.Drawer.DrawPos;
            drawPos.y = AltitudeLayer.Blueprint.AltitudeFor();
            int num2 = Find.TickManager.TicksGame - this.lastAbsorbDamageTick;
            if (num2 < 8)
            {
                float num3 = (float)((double)(8 - num2) / 8.0 * 0.0500000007450581);
                drawPos += this.impactAngleVect * num3;
                num1 -= num3;
            }
            float angle = (float)Rand.Range(0, 360);
            Vector3 s = new Vector3(num1, 1f, num1);
            Matrix4x4 matrix = new Matrix4x4();
            matrix.SetTRS(drawPos, Quaternion.AngleAxis(angle, Vector3.up), s);
            Graphics.DrawMesh(MeshPool.plane10, matrix, RangedShieldBelt.BubbleMat, 0);
        }

        public override bool AllowVerbCast(Verb verb)
        {
            return !(verb is Verb_LaunchProjectile);
        }
    }
}
