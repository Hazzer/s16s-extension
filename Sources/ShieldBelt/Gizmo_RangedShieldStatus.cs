﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace s16_extension
{
    [StaticConstructorOnStartup]
    internal class Gizmo_RangedShieldStatus : Gizmo
    {
        private static readonly Texture2D FullShieldBarTex = SolidColorMaterials.NewSolidColorTexture(new Color(0.2f, 0.2f, 0.24f));
        private static readonly Texture2D EmptyShieldBarTex = SolidColorMaterials.NewSolidColorTexture(Color.clear);
        public RangedShieldBelt shield;

        public Gizmo_RangedShieldStatus()
        {
            this.order = -100f;
        }

        public override float GetWidth(float maxWidth)
        {
            return 140f;
        }

        public override GizmoResult GizmoOnGUI(Vector2 topLeft, float maxWidth, GizmoRenderParms parms)
        {
            Rect overRect = new Rect(topLeft.x, topLeft.y, this.GetWidth(maxWidth), 75f);
            Find.WindowStack.ImmediateWindow(984688, overRect, WindowLayer.GameUI, (Action)(() =>
            {
                Rect rect1 = overRect.AtZero().ContractedBy(6f);
                Rect rect2 = rect1;
                rect2.height = overRect.height / 2f;
                Text.Font = GameFont.Tiny;
                Widgets.Label(rect2, this.shield.LabelCap);
                Rect rect3 = rect1;
                rect3.yMin = overRect.height / 2f;
                float fillPercent = this.shield.Energy / Mathf.Max(1f, this.shield.GetStatValue(StatDefOf.EnergyShieldEnergyMax, true));
                Widgets.FillableBar(rect3, fillPercent, Gizmo_RangedShieldStatus.FullShieldBarTex, Gizmo_RangedShieldStatus.EmptyShieldBarTex, false);
                Text.Font = GameFont.Small;
                Text.Anchor = TextAnchor.MiddleCenter;
                Rect rect4 = rect3;
                float num = this.shield.Energy * 100f;
                string str1 = num.ToString("F0");
                num = this.shield.GetStatValue(StatDefOf.EnergyShieldEnergyMax, true) * 100f;
                string str2 = num.ToString("F0");
                string label = str1 + " / " + str2;
                Widgets.Label(rect4, label);
                Text.Anchor = TextAnchor.UpperLeft;
            }), true, false, 1f);
            return new GizmoResult(GizmoState.Clear);
        }
    }
}
