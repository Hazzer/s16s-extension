# S16's Extension

## About
Decompiled dlls from original S16's Extension, done some cleanup and adjustmnet for Rimworld 1.3. Created open source project with S16 permission.

For more info check: https://www.loverslab.com/topic/148600-mod-s16s-extension/

## Linking reference
In visual studio: Project -> s16-extension Properties -> Reference Paths -> add `<SteamDirectory>\steamapps\common\RimWorld\RimWorldWin64_Data\Managed\`

Resolve harmony reference via nuget

## Credists
* S16 - Author
* Ed86 and RJW developers - RJW
* LoonyLadle - Apparel Hediff framework - https://ludeon.com/forums/index.php?topic=49129.0
* Ryflamer - HD head textures - https://steamcommunity.com/sharedfiles/filedetails/?id=1951826884
* Tarojun - hediff spawning things code - https://steamcommunity.com/sharedfiles/filedetails/?id=1632244750
* RicoFox233 - body textures - https://steamcommunity.com/sharedfiles/filedetails/?id=1905332152
* oblivionfs - testing
* Taranchuk - helping with C#
* RatherNot - C#, fixing bugs
* Abraxas - patching
* ShauaPuta - patching
* Monti - art
* dninemfive - ranged shield belt code
* Drahira - patron
* Jahazz - patron
* LPGaming - patron
* 152mmlSU  - patron
* Donald Moysey - patron
* Xander Draft - patron
* 刘益铭 - patron
* Quazar - patron
* Mori - patron 
